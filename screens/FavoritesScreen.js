import React from 'react';
import { View, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton';
import ProductList from '../components/ProductList';
import DefaultText from '../components/DefaultText';



/**Set all the favorite products in a List so I can view all the products I like.
 * If no product is inside favorite it will display a DefaultText with a message. */
const FavoritesScreen = props => {
    const favoriteProducts = useSelector(state => state.products.favoriteProducts);
    if (favoriteProducts.length === 0 || !favoriteProducts) {
        return (
            <View style={styles.content}>
                <DefaultText>No products found.</DefaultText>
            </View>

        );

    } else
    return <ProductList listData={favoriteProducts} navigation={props.navigation}

    />

};


/**Set an option Icon in the left top of the header so the user can navigate to other options. */
FavoritesScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Your Wishlist',
        headerLeft: (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Menu'
                    iconName='ios-menu'
                    onPress={() => {
                        navData.navigation.toggleDrawer()
                    }}
                />
            </HeaderButtons>
        )
    };
};

const styles = StyleSheet.create({
    content: {
        flex: 1, 
        justifyContent: 'center',
        alignItems: 'center'
    }
});

/**Export my class so it can be called in other classes. */
export default FavoritesScreen;