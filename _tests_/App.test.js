import React from 'react'
import {shallow} from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Renderer from 'react-test-renderer';



test('Test the test', () => {
    expect(2 + 2).toBe(4);
});


function compileAndroidCode() {
    throw new Error('you are using the wrong JDK');
}
test('compiling android goes as expected', () => {
    expect(compileAndroidCode).toThrow();
    expect(compileAndroidCode).toThrow(Error);

    // You can also use the exact error message or a regexp
    expect(compileAndroidCode).toThrow('you are using the wrong JDK');
    expect(compileAndroidCode).toThrow(/JDK/);
});


/*const wait = jest.fn();

test('wait', () => {
    expect(wait).toHaveBeenCalledTimes(1);
});



/*const HeaderButton = require('../components/HeaderButton');

describe('MyComponent', () => {
    it('should render correctly HeaderButton', () => {
        const component = shallow(<HeaderButton debug />);

        expect(component).toMatchSnapshot();
    });
});
*/

/*it('render correctly text component', () => {
    const Buybutton = Renderer.create(<BuyButton />).toJSON();
    expect(Buybutton).toMatchSnapshot();
});*/

