# TechView

## Setup

1. Install npm
2. Install expo
3. Install Xcode for iOS 
4. Install Android Studio for Android
5. Install a device in Android Studio with Android Virtual Device Manager

## Start Techview

1. Start an Android device with Android Virtual Device Manager
2. Start expo with `npm start` in the project root
3. Press i to run Techview on the iOS Simulator
4. Press a to run Techview on the Android Simulator
