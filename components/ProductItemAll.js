import React from 'react';
import {View, Text, TouchableOpacity, ImageBackground} from 'react-native';
import DefaultStyle from "../constants/default-style";


/**My productItem for all items component. */
const ProductItemAll = props => {

    return (
        <View
            style={DefaultStyle.productItem}>
            <TouchableOpacity
                onPress={props.onSelectProduct}

            >

                <View
                    style={{...DefaultStyle.productRow, ...DefaultStyle.productHeader}}>
                    <ImageBackground source={{uri: props.imageUrl}} style={DefaultStyle.bgImage}>
                        <View style={DefaultStyle.titleContainer}>
                            <Text
                                style={{...DefaultStyle.title,...DefaultStyle.productTitle}}
                                numberOfLines={1}>
                                {props.productName}
                            </Text>
                        </View>
                    </ImageBackground>
                </View>
                <View style={{height: 30, backgroundColor:'white', flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'}} >
               <Text style={{fontWeight: 'bold'}}>   minimum price:€{props.minPrice}                     maximum price:€{props.maxPrice}</Text>

                </View>

            </TouchableOpacity>
        </View>
    )

};



export default ProductItemAll;