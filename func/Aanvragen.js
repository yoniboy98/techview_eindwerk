import React from 'react';
import {SQLite} from "expo-sqlite";

/**Set connection for my api */
const API = "http://81.164.166.118/techviewApi_war_exploded/api/";

/**set the right link to connect on id */
export const findCommentById = async (id) => {
    let request = await fetch(API + "comments/id?id=" + id);
    return await request.json();
};
/**set the right link to connect on productId */
export const findCommentByproductId = async (productId) => {
    let request = await fetch(API + "comments/productId?productId=" + productId);
    return await request.json();
};
/**set the right link to connect on all comments*/
export const findAllComments = async () => {
    let request = await fetch(API + "comments/all");
    return await request.json();
};

/**Set data function
 * author: Maxime Esnol*/
export const getTextDate = (dbDateFormat) => {
    let parsedDate = dbDateFormat.substr(0, 10);
    let dateParts = parsedDate.split("-"),
        year =  dateParts[0],
        month = dateParts[1],
        day =   dateParts[2],
        date =  new Date();

    date.setFullYear(year, month, day);
    let textDateParts = date.toString().split(" ");

    return "" + textDateParts[2]  //the day number
        + " " + textDateParts[1]  //the month abbreviation
        + " " + textDateParts[3]; // the year number
}



export const addCommentDb = async () => {
    let request = await fetch (API+ "comment/add");
    return await request.json();
};
export const deleteCommentDb = async () => {
    let request = await fetch (API+ "comment/delete");
    return await request.json();
};



/*const runQuery = async (db, query, params = []) => {
    return new Promise((resolve, reject) => {
        db.transaction(
            transaction => {
                transaction.executeSql(
                    query,
                    params,
                    (_, { rows: { _array } }) => {
                        resolve(_array);
                    }
                )
            },
            error => {
                reject(error);
            }
        );
    });
};*/



/**Search on productName , processor name , software , storage , minprice and maxprice in Json file "PRODUCTS" while looping over the JSON file. */
export const findProductNameLike = text => {
    let products = require("../assets/PRODUCTS");
    let foundProducts = [];

    for (let i = 0; i < products.length; i++) {
        let product = products[i];
        if (products[i].productName.toLowerCase().includes(text.toLowerCase())
            ||
            products[i].processor.toLowerCase().includes(text.toLowerCase())
            ||
            products[i].software.toLowerCase().includes(text.toLowerCase())
            ||
            products[i].storage.toLowerCase().includes(text.toLowerCase())
            ||
            products[i].minPrice.toLowerCase().includes(text.toLowerCase())
            ||
            products[i].maxPrice.toLowerCase().includes(text.toLowerCase())) {

            foundProducts.push(product);

        }
    }

    return foundProducts;
}
/** Search on categoryName (title) in Json file CATEGORY while looping over it.*/
export const findCategoryName = title => {
    let category = require("../assets/CATEGORY");

    let foundCategories = [];

    for (let i = 0; i < category.length; i++) {
        let categories = category[i];
        if (category[i].title.toLowerCase().includes(title.toLowerCase())) {

            foundCategories.push(categories);

        }
    }

    return foundCategories;
}

/*
export const getDB = () => {
    return SQLite.openDatabase("techview-db");
};


export const initDb = async (db) => {
  await runQuery(db,
        "CREATE TABLE IF NOT EXISTS comments (id INTEGER PRIMARY KEY autoincrement , comment text NOT NULL, productId varchar(20) NOT NULL)");

};




export const addComment = async (db,text, id) => {
 await runQuery(
        db,
        "INSERT INTO comments (comment,productId) VALUES (?,?)",
        [text,id]

    );

    console.log("added");
};


export const removeComment = async (db, id) => {
    await runQuery(
        db,
        "DELETE FROM comments WHERE id = ?",
        [id]
    );
};



export const getAllComments = async (db,id) => {
    let rows = await runQuery(
        db,
        "SELECT * FROM comments where productId = ? ORDER BY id DESC ",
        [id]
    );
    return rows;
};

*/
