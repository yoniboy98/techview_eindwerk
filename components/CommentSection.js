import React, {useState} from 'react';
import {View, Text, StyleSheet ,TextInput} from 'react-native';
import DefaultStyle from "../constants/default-style";
import {addComment, getDB} from "../func/Aanvragen";
import SendCommentButton from "./SendCommentButton";




/** My CommentSection component */
const CommentSection = props => {
    const [text,setText] = useState("");

        let db = getDB();

    return (
        <View>
<TextInput style={DefaultStyle.CommentBox}
           placeholder="describe your experience with this article"
           autoFocus={false}
           editable
            onChangeText={(text) => setText(text)}

/>

<SendCommentButton onPress={() => addComment(db,props.id,text)}/>




        </View>
    )}

    export default CommentSection;