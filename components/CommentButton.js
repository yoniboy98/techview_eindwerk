import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {  Text } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import AwesomeButtonCartman  from 'react-native-really-awesome-button/src/themes/cartman';


/** my CommentButton component */
const CommentButton = props => {
    return(

        <TouchableOpacity >

            <AwesomeButtonCartman style={{paddingBottom: 12}}
                                  width={130}
                                  onPress={props.onPress}
                              >


                <Text>  <Ionicons name="ios-clipboard" size={27}/> COMMENTS   </Text></AwesomeButtonCartman>

        </TouchableOpacity>
    );
};


export default CommentButton;