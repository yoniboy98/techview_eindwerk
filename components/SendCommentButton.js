import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {  Text } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import AwesomeButtonCartman  from 'react-native-really-awesome-button/src/themes/cartman';

/** my CommentButton component */

const SendCommentButton = props => {
    return(

        <TouchableOpacity >

            <AwesomeButtonCartman style={styles.btnContainer}
                                  width={100}

            >


                <Text>  <Ionicons name="ios-send" size={24} color={"blue"}/> SEND   </Text></AwesomeButtonCartman>

        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    btnContainer: {
paddingTop: 10,
 marginLeft: '35%'
    },

});


export default SendCommentButton;