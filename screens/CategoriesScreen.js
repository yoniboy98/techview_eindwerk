import React, {useCallback, useState} from 'react';
import {FlatList, RefreshControl} from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../components/HeaderButton';
import CategoryGridTile from '../components/CategoryGridTile';




/** Enable refresh function*/
function wait(timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}

/** Call json file CATEGORY*/
let category = require('../assets/CATEGORY');


/** Render all products that are in my Json file and navigate to the next screen "CategoryProductScreen" on id.
 * Show category title and photo(photoLink)*/
const CategoriesScreen = props => {

    const renderGridItem = itemData => {
        return <CategoryGridTile
            title={itemData.item.title}
            photoLink={itemData.item.photoLink}
            onSelect={() => {
                props.navigation.navigate({
                    routeName: "Category", params: {
                        categoryId: itemData.item.id
                    }
                });
            }} />;
    };

    /** Keeping old values when refreshing.*/
    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = useCallback(() => {
        setRefreshing(true);

        wait(500).then(() => setRefreshing(false));
    }, [refreshing]);



    /** Show all objects from my Json file
     * set a key to each object so there will be no duplicates*/
    return (
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh}>
        <FlatList
            keyExtractor={(item, index) => item.id}
            data={category}
            renderItem={(item) => renderGridItem(item)}
            numColumns={1}
        />
        </RefreshControl>
    );
};

/**Set an option Icon in the left top of the header to navigate to other screens. */
CategoriesScreen.navigationOptions = navData => {
    return {
        headerLeft: (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Menu'
                    iconName='ios-options'
                    onPress={() => {
                        navData.navigation.toggleDrawer();
                    }}
                />
            </HeaderButtons>
        )
    };
};


/**Export my class so it can be called in other classes. */
export default CategoriesScreen;