
/**The main colors I will use in my application. */
export default {
    primaryColor: '#052F5F',
    accentColor: '#F1A208'
};